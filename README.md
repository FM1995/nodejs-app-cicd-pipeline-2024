# nodejs-app-cicd-pipeline project 2024


#### Project Outline

Our company decided that they will use AWS as a cloud provider to deploy their applications. It's too much overhead to manage multiple platforms, including the billing etc.

So you need to deploy the previous NodeJS application on an EC2 instance now. This means you need to create and prepare an EC2 server with the AWS Command Line Tool to run your NodeJS app container on it.
We will also Set up Continuous Deployment, where we don’t have to deploy new images manually, we will also include versioning and webhooks to speed up the delivery of the deployment of the dockerized application.

![Project Outline](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Project_Outline.png)


## Table of Contents
- [Create IAM user](#create-iam-user)
- [Configure AWS CLI](#configure-aws-cli)
- [Create VPC](#create-vpc)
- [Create EC2 Instance](#create-ec2-instance)
- [SSH into the server and install Docker on it](#ssh-into-the-server-and-install-docker-on-it)
- [Add docker-compose for deployment](#add-docker-compose-for-deployment)
- [Configure access from browser via EC2 Security Group](#configure-access-from-browser-via-ec2-security-group)
- [Configure automatic triggering of multi-branch pipeline](#configure-automatic-triggering-of-multi-branch-pipeline)

## Links to finalised code used in this project
[main branch](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline)

[jenkins-jobs](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline/-/tree/jenkins-jobs?ref_type=heads)

[dev branch](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline/-/tree/Dev/Fixes?ref_type=heads)



## Create IAM user

First of all, we need an IAM user with correct permissions to execute the tasks below.
Let’s a new IAM use with  "your name" with "devops" user-group
Give the "devops" group all needed permissions to execute the tasks below - with login and CLI


```
aws iam create-user --user-name FuadAWS   //Create user with username
aws iam create-group --group-name devops  //Create group
aws iam add-user-to-group --user-name FuadAWS --group-name devops   //Add user to group
aws iam get-group --group-name devops  //Verify that the group has my user and username
```

![Image1](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image1.png)

Next lets Configure UI and CLI access

```
aws iam create-access-key --user-name FuadAWS > fuadawskey.txt //Generate the access and secret keys and store in a location
```

![Image2](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image2.png)

```
aws iam create-login-profile --user-name FuadAWS --password BootcampProject123 // Generate user login credentials for UI
```

![Image3](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image3.png)

```
aws iam list-policies | grep ChangePassword //Give user permission to change password
```

![Image4](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image4.png)


```
aws iam attach-user-policy --user-name FuadAWS --policy-arn "arn:aws:iam::aws:policy/IAMUserChangePassword"
```

![Image5](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image5.png)

Logging into the UI

And can login successfully

![Image6](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image6.png)

Next we need to Assign permissions to the user through group for VPC and EC2 access

We can utilise the below commands to check which policies are available and can be applied

```
aws iam list-policies | grep EC2FullAccess
aws iam list-policies | grep VPCFullAccess
```

![Image7](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image7.png)

Using the above policy name for EC2

```
aws iam attach-group-policy --group-name devops --policy-arn "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
aws iam attach-group-policy --group-name devops --policy-arn "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
```

And can see the polcies are attached to the group

![Image8](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image8.png)

## Configure AWS CLI

We want to use the AWS CLI for the following tasks. So, to be able to interact with the AWS account from the AWS Command Line tool you need to configure it correctly:
Set credentials for that user for AWS CLI
Configure correct region for your AWS CLI
To achieve this we can do the following
Cat the previously created keypair file and run aws configure to input the access keys


![Image9](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image9.png)

Using the below to verify the user I am using that has been created

![Image10](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image10.png)

## Create VPC

We want to create the EC2 Instance in a dedicated VPC, instead of using the default one. So we:

create a new VPC with 1 subnet and
create a security group in the VPC that will allow you access on ssh port 22 and will allow browser access to your Node application
(using the AWS CLI)
Using the documentation below, we can create a vpc with cidr block 10.0.0.0/16

https://docs.aws.amazon.com/vpc/latest/userguide/create-vpc.html#create-vpc-cli

```
aws ec2 create-vpc --cidr-block 10.0.0.0/16 //Create VPC
```

![Image11](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image11.png)

Next create the subnet for that vpc

This will create the subnet using the vpc id

```
aws ec2 create-subnet --vpc-id vpc-09a5e51c1eeface87 --cidr-block 10.0.1.0/24
```

![Image12](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image12.png)

Next we will create a security group in the VPC that will allow you access on ssh port 22 and will allow browser access to our Node application and create an internet gateway

```
aws ec2 create-internet-gateway
```

![Image13](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image13.png)


Next we will attach the gateway to the vpc id

```
aws ec2 attach-internet-gateway --vpc-id vpc-09a5e51c1eeface87 --internet-gateway-id igw-0aa825b1de09f636a
```

And then validating

```
aws ec2 describe-internet-gateways --filters "Name=attachment.vpc-id,Values=vpc-09a5e51c1eeface87"
```

![Image14](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image14.png)

Next we will create a route table for the vpc

```
aws ec2 create-route-table --vpc-id vpc-09a5e51c1eeface87
```

![Image15](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image15.png)

Now we will create route rule for handling all traffic between internet & VPC

```
aws ec2 create-route --route-table-id rtb-0fade57ab348e8572 --destination-cidr-block 0.0.0.0/0 --gateway-id igw-0aa825b1de09f636a
```

![Image16](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image16.png)

Now lets associate subnet with the route table to allow internet traffic in the subnet as well

```
aws ec2 associate-route-table  --subnet-id subnet-09402fcc4a4d72308 --route-table-id rtb-0fade57ab348e8572
```

![Image17](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image17.png)

Next step we will create a security group in the VPC that will allow you access on ssh port 22 and will allow browser access to your Node application, so the below needs to be achieved

```
aws ec2 create-security-group --group-name FuadSG --description "Security group for SSH access" --vpc-id vpc-09a5e51c1eeface87
```

![Image18](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image18.png)

Next we will add incoming access on port 22 from all sources to security group

```
aws ec2 authorize-security-group-ingress --group-id sg-0646ffdc1fe79872e --protocol tcp --port 22 --cidr 0.0.0.0/0
```

![Image19](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image19.png)


## Create EC2 Instance

Once the VPC is created we can create an EC2 instance in that VPC with the security group we just created and ssh key file 
We can utilise the below command

```
aws ec2 create-key-pair --key-name EC2KeyPair --query "KeyMaterial" --output text > EC2KeyPair.pem
```

Would then need to set the keypair file to read only

```
chmod 400 EC2KeyPair.pem
```

![Image20](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image20.png)

Now ready to create the EC2 using the below template

![Image21](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image21.png)

Can grab the ami from management console which is the image id

![Image22](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image22.png)

Now with the previous details we have created we can now construct the ec2 server using the cli

```
aws ec2 run-instances --image-id ami-0b594cc165f9cddaa --count 1 --instance-type t2.micro --key-name EC2KeyPair --security-group-ids sg-0646ffdc1fe79872e --subnet-id subnet-09402fcc4a4d72308 --associate-public-ip-address
```

![Image23](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image23.png)

Can see the instance id - i-0fb69857fdb60e0fa

Now lets validate and see if it is in a running state and has a public ip address

```
aws ec2 describe-instances --instance-id i-0fb69857fdb60e0fa --query "Reservations[*].Instances[*].{State:State.Name,Address:PublicIpAddress}"
```

![Image24](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image24.png)

Can also validate in the UI

![Image25](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image25.png)

## SSH into the server and install Docker on it

Once the EC2 instance is created successfully, we want to prepare the server to run Docker containers. So we ssh into the server and install Docker on it to run the dockerized application later
We will ssh into EC2 instance using th epublic IP address we got earlier

```
ssh -i "EC2KeyPair.pem" ec2-user@18.133.77.96
```

![Image26](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image26.png)

Now ready to install docker

```
sudo yum update -y
sudo yum install -y docker
```

![Image27](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image27.png)

Checking docker version

![Image28](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image28.png)

Checking the groups and see if the ec2 user can do docker commands

![Image29](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image29.png)

```
sudo usermod -aG docker ec2-user
```

Logout and then log back in

![Image30](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image30.png)

## Add docker-compose for deployment

We will add a docker-compose to our nodejs application

Now ready to create the docker-compose.yaml file

![Image31](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image31.png)

![Image32](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image32.png)

## Adding "deploy to EC2" step to your pipeline

We will modify our previous project to include a stage to deploy to ec2 instead of deploy to server

[Jenkinsfile](https://gitlab.com/FM1995/nodejs-app-ci-pipeline/-/blob/master/Jenkinsfile)

First lets create the EC2 user in Jenkins with the key pair contents

![Image33](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image33.png)

![Image34](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image34.png)

Next lets install docker-compose on the ec2 server

![Image35](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image35.png)

Displaying docker compose version

![Image36](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image36.png)

Also creating the docker.sh for the ec2 instance to complete docker commands
In summary, the script sets up the Docker services defined in the docker-compose.yaml file and runs them in detached mode, and then it prints "success" to indicate that the process has completed successfully.

![Image37](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image37.png)

Can now modiy the Jenkinsfile where the below are the contents

echo "deploying docker image to EC2!": Prints a message indicating the start of the deployment process.

def shellCmd = "bash ./docker.sh ${IMAGE_NAME}": Defines a shell command (shellCmd) to run on an EC2 instance. Constructs a command to execute the docker.sh script, passing the value of the IMAGE_NAME environment variable as an argument.

sshagent(['EC2-User']) { ... }: Initiates the SSH agent using the credentials named EC2-User, allowing subsequent SSH-related steps to use these credentials for authentication.

sh 'scp docker.sh ec2-user@18.133.77.96:/home/ec2-user': Uses SCP to copy the docker.sh script from the local Jenkins workspace to /home/ec2-user on the remote EC2 instance with the IP address 18.133.77.96.

sh 'scp docker-compose.yaml ec2-user@18.133.77.96:/home/ec2-user': Similar to the previous step, copies the docker-compose.yaml file to the same location on the remote EC2 instance.

sh "ssh -o StrictHostKeyChecking=no ec2-user@18.133.77.96 ${shellCmd}": Initiates an SSH connection to the remote EC2 instance (18.133.77.96) using the ec2-user account. The -o StrictHostKeyChecking=no flag disables strict host key checking. The ${shellCmd} variable contains the command to run the docker.sh script with the value of ${IMAGE_NAME} as an argument.

![Image38](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image38.png)

Configuration change for Jenkins

![Image39](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image39.png)

Now ready for build

![Image40](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image40.png)

![Image41](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image41.png)

Build is now successful
Also checking the docker repo
Can see the new version aswell as the build number

![Image42](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image42.png)

## Configure access from browser via EC2 Security Group

After executing the Jenkins pipeline successfully, the application is deployed, but you still can't access it from the browser. Again, you need to open the correct port on the server. For that you:

Configure EC2 security group to access your application from browser (using AWS CLI)
Lets allow the application to be accessed via port 3000 and all IP’s on the security group
Can be done via the UI or CLI

```
aws ec2 authorize-security-group-ingress --group-id sg-0646ffdc1fe79872e --protocol tcp --port 3000 --cidr 0.0.0.0/0
```

![Image43](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image43.png)

Checking the UI

![Image44](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image44.png)

Now able to access the app on the public IP - 18.133.77.96 with port 3000
Able to now access the UI

![Image45](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image45.png)

## Configure automatic triggering of multi-branch pipeline

Our team members are creating branches to add new features to the application or fix stuff, so you don't want to build and deploy all these half-done features or bug fixes. You want to build and deploy only the master branch. All other branches should only run tests. Add this logic to the Jenkinsfile. So we will add branch based logic to Jenkinsfile and Add webhook to trigger pipeline automatically

Lets proceed 

We will add the condition only allow main branch to build and deploy application

![Image46](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image46.png)

![Image47](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image47.png)

Can see it skips the stages intended for the master branch for the Jenkins-jobs branch

![Image48](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image48.png)

And the same for the master branch where it builds out the intended stage

![Image49](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image49.png)

Testing another branch ‘Dev/Fixes’

![Image50](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image50.png)

Now for the webhook for the nodejs-app multibranch

First install Multibranch scan webhook trigger

![Image51](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image51.png)

Can then Go on Gitlab Webhooks and configure it to trigger on push events. You will do this by adding the URL in step 3

![Image52](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image52.png)

Using the provided command

```
JENKINS_URL/multibranch-webhook-trigger/invoke?token=[Trigger token]
```

![Image53](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image53.png)

Now lets test

Previous

![Image54](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image54.png)

New

![Image55](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image55.png)

Can see it has now been triggered

![Image56](https://gitlab.com/FM1995/nodejs-app-cicd-pipeline-2024/-/raw/main/Images/Image56.png)

















